**L’outil OCARHY**

OCARHY est un outil SIG mutualisé pour la gestion des milieux aquatiques. Il est a destination de toutes structures GEMAPI souhaitant améliorer l’exploitation de leurs données sur leur territoire. OCARHY est adressé à un public. L’objectif de l’outil est de permettre aux utilisateurs de pouvoir saisir de la donnée relative aux milieux aquatiques. De pouvoir accéder à un large catalogue de données et de pouvoir extraire ces données pour produire rapidement des diagrammes ou des cartes. 

**Le contenu de la solution**

La solution est une base de données qui mutualise de nombreuses ressources produites par des acteurs nationaux comme l’IGN, la SANDRE ou encore l’INPN. Celle-ci contient aussi des données en lien avec les besoins métiers des techniciens, des tables pour réaliser de la saisie de diagnostic ou du suivi des actions de gestion. Cette base est paramétrée avec des triggers, des contraintes ainsi que des droits d’utilisateurs limitants la saisie des techniciens uniquement à leur territoire d’actions.

Pour interagir avec la base, le logiciel QGIS est exploité. Son interface a été customisée et un projet personnalisé a été créé. Pour réaliser une saisie de donnée, des formulaires ont été mis en place pour simplifier la saisie de chaque donnée. Enfin, grâce à l'extension DataPlotly, il est possible de mettre en place des bilans comportant des graphique et des cartes dynamiques en fonction d'une entité géographique sélectionnée. 

**L’installation et l’utilisation**

Pour installer l'outil et l'utiliser, vous pouvez suivre les indications dans le **Guide utilisateur**.  
Pour comprendre comment l'outil a été développé vous pouvez vous référer au **Guide Administrateur**. Ce dernier explique les différentes étapes pour gérer la base de données et administrer les paquets d'installation.

----

## Scénario pour QGIS Deployment Toolbelt (QDT)

Un scénario pour l'outil [QGIS Deployment Toolbelt](https://guts.github.io/qgis-deployment-cli/usage/installation.html (version 0.9.0 à date) est disponible dans le dossier `qdt_scenarii`.

Pour l'utiliser :

1. télécharger l'exécutable depuis les versions publiées sur GitHub (attention, ces exécutables ne sont pas signés car génériques) : <https://github.com/Guts/qgis-deployment-cli/releases/latest>
1. télécharger le [fichier de scénario](qdt_scenarii/scenario.qdt.yml) à côté de l'exécutable
1. double-cliquer sur l'exécutable pour lancer le scénario

Il permet de télécharger automatiquement les profils OCARHY et de les installer dans les profils QGIS de l'utilisateur final, puis de créer un raccourci (bureau + menu Démarrer) pour chacun des profils.

:warning: A date (mi-mai 2022, l'outil est toujours en phase de développement actif. Les fonctionnalités et syntaxes sont donc potentiellement amenées à changer.
