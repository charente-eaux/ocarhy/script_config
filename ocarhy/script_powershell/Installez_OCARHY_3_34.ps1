﻿<#
Logiciel : 
- PowerShell ISE Natif (tu pourrais le faire sur Visual Code studio autrement)
- Et j'execute sur un cmd Powershell à coté [Toujours en tant qu'administrateur] (il y a quelques erreurs en executant directement sur ISE)

Note d'usage :

- Vérifier sur la ligne 31 que c'est bien la version de QGIS que vous préconisez
- Run les scripts ps1 depuis la console powershell (en déclarant en cd le lieu où se trouve votre script ps1). Cela évitera les bugs avec $pwd et $PSScriptRoot (ca bug avec un run ISE direct)
- Bien communiquer qu'ils doivent installer sur programm Files
- Quand on créé les paquets, bien supprimer le auth-db
#>

#Transformation en .exe : Il faut transformer ce script en un executable

#Sur un nouveau PC : Installer les modules (il faut bypass certains droits en fonction du PC

<#
Install-Module ps2exe
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass
Import-Module ps2exe
#>

<#
Set-Variable :
permet de créer une variable

-Name : nom de la variable
-Value : valeur de la variable

Get-Variable :
affiche la variable appelée (c'est utile en développement pour voir ou ça a bugué)
#>

<# Get-ChildItem :²
Cherche un object dans un répertoire donné (il génère une liste s'il y a plusieurs objects trouvés)

- Filter : Définit le nom de l'objet
- Directory : Spécifie que l'on cherche un répertoire (et non un fichier)
- Select-Object : Permet de sélectionner un Objet dans la liste donnée dans le Get-childItem
- First 1 : On extrait que le premier objet de la liste

$qgis_version :
On créé une variable contenant le dossier dans les paramètres de l'utilisateur qui commence par 3.22 (d'où l'utilisation du *)
#>

<# 
[Environment]::GetFolderPath("Desktop")
Permet d'avoir le chemin vers le bureau du PC (il y a une version raccourcie qui existe je suppose)

Création d'une variable pour extraire le chemin du bureau de l'utilisateur
#>

<#
On cherche la version de QGIS installée
Ne pas oublier le * car les fichiers ont toujours des numéros de sous-versions
#>

$qgis_version=$null
Get-Variable -Name "qgis_version"

<#
if {} :
Boucle conditionnelle if

-ne : N'est pas égal (non-egal)
-eq : est égal (ou équivalent)
$null : Variable (par défaut de powershell) contenant une valeur nulle
#>


if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.34*" -Directory "C:\Program Files\"  | Select-Object -First 1
$qgis_ltr=Get-ChildItem -Filter "QGIS 3.34*" -Directory "C:\Program Files\$qgis_version\bin\qgis-ltr-bin.exe"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
if ($qgis_ltr -eq $null)
{Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}else{
Set-Variable -Name "ltr" -Value "1"
Get-Variable -Name "ltr"  
}
}


if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.36*" -Directory "C:\Program Files\"  | Select-Object -First 1
$qgis_ltr=Get-ChildItem -Filter "QGIS 3.36*" -Directory "C:\Program Files\$qgis_version\bin\qgis-ltr-bin.exe"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
if ($qgis_ltr -eq $null)
{Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}else{
Set-Variable -Name "ltr" -Value "1"
Get-Variable -Name "ltr"  
}
}


if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.32*" -Directory "C:\Program Files\"  | Select-Object -First 1
$qgis_ltr=Get-ChildItem -Filter "QGIS 3.32*" -Directory "C:\Program Files\$qgis_version\bin\qgis-ltr-bin.exe"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
if ($qgis_ltr -eq $null)
{Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}else{
Set-Variable -Name "ltr" -Value "1"
Get-Variable -Name "ltr"  
}
}


if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.28*" -Directory "C:\Program Files\"  | Select-Object -First 1
$qgis_ltr=Get-ChildItem -Filter "QGIS 3.28*" -Directory "C:\Program Files\$qgis_version\bin\qgis-ltr-bin.exe"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
if ($qgis_ltr -eq $null)
{Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}
else
{
Set-Variable -Name "ltr" -Value "1"
Get-Variable -Name "ltr"  
}
}

if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.30*" -Directory "C:\Program Files\"  | Select-Object -First 1
$qgis_ltr=Get-ChildItem -Filter "QGIS 3.22*" -Directory "C:\Program Files\$qgis_version\bin\qgis-ltr-bin.exe"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
if ($qgis_ltr -eq $null)
{Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}else{
Set-Variable -Name "ltr" -Value "1"
Get-Variable -Name "ltr"  
}
}


if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.26*" -Directory "C:\Program Files\"  | Select-Object -First 1
Get-Variable -Name "qgis_version"

if ($qgis_ltr -ne $null -and $qgis_version -eq $null)
{
Set-Variable -Name "ltr" -Value "0"
Get-Variable -Name "ltr"
}
}

if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.20*" -Directory "C:\Program Files\"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
Set-Variable -Name "ltr" -Value "1"
}

if ($qgis_version -eq $null)
{ $qgis_version=Get-ChildItem -Filter "QGIS 3.18*" -Directory "C:\Program Files\"  | Select-Object -First 1
Get-Variable -Name "qgis_version"
Set-Variable -Name "ltr" -Value "0"
}

<#
Il se peut qu'il ai fait son installation via OSGEO4W, du coup il faut qu'on regarde si il a fait son installation ici
#>

if ($qgis_version -eq $null)
{ $verif=Test-Path -Path "C:\OSGeo4W\bin\qgis-ltr-bin.exe"
Get-Variable -Name "verif"
if ($verif -eq $true -and $qgis_version -eq $null)
{
Set-Variable -Name "qgis_version" -Value "V.OSGEO"
Set-Variable -Name "ltr" -Value "3"
}
}

if ($qgis_version -eq $null)
{ $verif_no=Test-Path -Path "C:\OSGeo4W\bin\qgis-bin.exe"
Get-Variable -Name "verif_no"
if ($verif -eq $false -And $verif_no -eq $true -and $qgis_version -eq $null)
{ 
Set-Variable -Name "qgis_version" -Value "V.OSGEO"
Set-Variable -Name "ltr" -Value "2"
}
}

<#
On verifie si la variable $qgis-version a bien enregistrée une valeur
Cela permet de savoir si on lance le processus ou non.
#>

if ($qgis_version -ne $null)

{ <#
$pwd :
Variable pour récupérer la Current Directory (compatible avec les .exe)

On créé une variable qui nous trouve le dossier des profils dans le zip/le dossier
(C'est utile à voir dans la console)
#>

Set-Variable -Name "chemin_script" -Value $pwd
Get-Variable -Name "chemin_script"

<# 
$$Env:Programfiles :
Variable pour récupérer le chemin de programes files

$prog_file :
Contient le chemin de programes files (C'est pour visualiser dans la console, le chemin de l'utilisateur)
#>

Set-Variable -Name "prog_file" -Value $Env:Programfiles
Get-Variable -Name "prog_file"

<# 
$PSScriptRoot :
trouve le chemin du dossier actuel du script .ps1 mais ne marche que en forme de script (donc le passage en .exe va nous bloquer)

$root :
Stocke le chemin du script. (c'est uniquement utile pour visualiser la variable sur la console en développement)
#>

#Set-Variable -Name "root" -Value $PSScriptRoot
#Get-Variable -Name "root"

<# 
Création d'une variable pour extraire le chemin de l'executable "qgis-bin.exe".
On prend bien en compte des particularités plus hautes (si c'est ltr ou non et si c'est OSGEO ou son opposé)

$qgis_location : Où se trouve l'exe et qgis
$qgis_folder : Où se trouve le répertoire de qgis
#>

# Si la personne à une version non-stable de QGIS
if ($ltr -eq 0)
{ 
$qgis_location = “$prog_file\$qgis_version\bin\qgis-bin.exe”
Get-Variable -Name "qgis_location"

$qgis_folder = “$prog_file\$qgis_version\bin”
Get-Variable -Name "qgis_folder"
}

# Si la personne à une version stable de QGIS
if ($ltr -eq 1)
{ 
$qgis_location = “$prog_file\$qgis_version\bin\qgis-ltr-bin.exe”
Get-Variable -Name "qgis_location"

$qgis_folder = “$prog_file\$qgis_version\bin”
Get-Variable -Name "qgis_folder"
}

# Si la personne à une version OSGEO non-stable de QGIS
if ($ltr -eq 2)
{ 
$qgis_location = “C:\OSGeo4W\bin\qgis-bin.exe”
Get-Variable -Name "qgis_location"

$qgis_folder = “$prog_file\$qgis_version\bin”
Get-Variable -Name "qgis_folder"
}

# Si la personne à une version OSGEO stable de QGIS
if ($ltr -eq 3)
{
$qgis_location = “C:\OSGeo4W\bin\qgis-ltr-bin.exe”
Get-Variable -Name "qgis_location"

$qgis_folder = “$prog_file\$qgis_version\bin”
Get-Variable -Name "qgis_folder"
}

<# 
[Environment]::GetFolderPath("Desktop")
Permet d'avoir le chemin vers le bureau du PC (il y a une version raccourcie qui existe je suppose)

Création d'une variable pour extraire le chemin du bureau de l'utilisateur
#>
$DeskopChemin = [Environment]::GetFolderPath("Desktop")
Get-Variable -Name "DeskopChemin"

<#
$appdata :
On extrait le chemin de appdata
#>
Set-Variable -Name "appdata" -Value $env:APPDATA\QGIS\QGIS3\profiles\
Get-Variable -Name "appdata"

<#
$appdata :
On extrait le chemin de l'endroit avant appdata.
La raison c'est que le qgis_global_settings.ini doit être situé ici pour bien fonctionner
#>

Set-Variable -Name "appQGIS" -Value $env:APPDATA\QGIS\QGIS3\
Get-Variable -Name "appQGIS"

<#remove-Item :
On supprime des dossiers

- Recurse : On tue les parents mais aussi les enfants ! (donc ce qui est contenu dans les dossiers)
- ErrorAction Ignore : On ignore les messages d'erreurs (car ils s'afficheront dans la console et ça fait peur !

L'idée est de supprimer les profiles et le petit fichier .ini avant de les réinstaller proprement
ATTENTION : On perd tout du coup
#>

Remove-Item $appdata\ocarhy_bureau -Recurse -ErrorAction Ignore
Remove-Item $appdata\ocarhy_terrain -Recurse -ErrorAction Ignore
#Remove-Item $appdata\ocarhy_valorisation -Recurse -ErrorAction Ignore

#N'est pas nécessaire car il n'est censé être que présent dans $appQGIS
#mais au cas où (vis à vis d'une précedente installation défaillante, il est préférable de s'assurer qu'il ai disparu de $appdata)

Remove-Item $appdata\qgis_global_settings.ini -Recurse -ErrorAction Ignore
Remove-Item $appQGIS\qgis_global_settings.ini -Recurse -ErrorAction Ignore

Remove-Item $appdata\qgis_global_settings.ini -Recurse -ErrorAction Ignore
Remove-Item $appQGIS\startup.py -Recurse -ErrorAction Ignore


#DEZIPPAGE DU PAQUET DANS LE CHEMINS DES PROFILS QGIS
#-------------------------------------------------------------------------------------
<#Expand-Archive :
ouvrir une archive (zip)

-Path : Le chemin du zip
- DestinationPath : Le chemin où on décompresse
- Force : Même s'il y a des trucs déjà existants, on force lextraction à l'endroit donnée.
#>
Expand-Archive -Path $chemin_script\deploiement.zip -DestinationPath $appdata -Force

<#Move-Item : Couper-Colelr
On déplace le .ini qu'on a dézipper dans le dossier des profil dans le dossier juste avant (car c'est comme ça qu'il marche !)
#>
Move-Item -Path $appdata\qgis_global_settings.ini -Destination $appQGIS\qgis_global_settings.ini
Move-Item -Path $appdata\startup.py -Destination $appQGIS\startup.py

#CREATIONS DE CHEMINS DIVERS
#-------------------------------------------------------------------------------------
<#
On créé les chemins pour accéder aux chemins des presets de QGIS (là où sont stockés les .ini
#>
Set-Variable -Name "folder_bureau" -Value $appdata"ocarhy_bureau/QGIS/"
Set-Variable -Name "folder_terrain" -Value $appdata"ocarhy_terrain/QGIS/"
#Set-Variable -Name "folder_valorisation" -Value $appdata"ocarhy_valorisation/QGIS/"

<#
On créé les chemins pour accéder aux chemins des presets d'OCARHY
#>
Set-Variable -Name "preset_bureau" -Value $appdata"ocarhy_bureau/ocarhy/"
Set-Variable -Name "preset_terrain" -Value $appdata"ocarhy_terrain/ocarhy/"
#Set-Variable -Name "preset_valorisation" -Value $appdata"ocarhy_valorisation/ocarhy/"

<#
On créé les chemins pour accéder aux projet QGIS de chaque profils
Faites toujours attention aux noms de vos projets
#>

Set-Variable -Name "project_bureau" -Value $preset_bureau"OCARHY_B.qgz"
Set-Variable -Name "project_terrain" -Value $preset_terrain"OCARHY_T.qgz"
#Set-Variable -Name "project_valorisation" -Value $preset_valorisation"OCARHY_V.qgz"

Set-Variable -Name "project_bureau_quote" -Value """$project_bureau"""
Set-Variable -Name "project_terrain_quote" -Value """$project_terrain"""
#Set-Variable -Name "project_valorisation_quote" -Value """$project_valorisation"""

Set-Variable -Name "pythonpath_bureau" -Value $preset_bureau"startup.py"
Set-Variable -Name "pythonpath_terrain" -Value $preset_terrain"startup.py"
#Set-Variable -Name "pythonpath_valorisation" -Value $preset_valorisation"startup.py"

<#
On récupère les chemins QGISCUSTOMIZATION3.ini
#>
Set-Variable -Name "custo_bureau" -Value $folder_bureau\QGISCUSTOMIZATION3.ini
Set-Variable -Name "custo_terrain" -Value $folder_terrain\QGISCUSTOMIZATION3.ini
#Set-Variable -Name "custo_valorisation" -Value $folder_valorisation\QGISCUSTOMIZATION3.ini


#PARAMETRAGE DES ICONES ET DES RACCOURCIS
#-------------------------------------------------------------------------------------

<#
Création du chemin des icones
#>
Set-Variable -Name "icon_bureau" -Value $preset_bureau"iconeb.ico"
Set-Variable -Name "icon_terrain" -Value $preset_terrain"iconet.ico"
#Set-Variable -Name "icon_valorisation" -Value $preset_valorisation"iconev.ico"


<#
Shortcut Bureau
#>
$ShortcutBureau = "$DeskopChemin\OCARHY Bureau.lnk"
$WScriptShell_b = New-Object -ComObject WScript.Shell

$Shortcut_b = $WScriptShell_b.CreateShortcut($ShortcutBureau)
$Shortcut_b.TargetPath = $qgis_location
$Shortcut_b.WorkingDirectory = "$qgis_folder"
$Shortcut_b.Arguments = "--profile ocarhy_bureau "+$project_bureau_quote
$shortcut_b.IconLocation=$icon_bureau
$Shortcut_b.Save()

<#
Shortcut Terrain
#>
$ShortcutTerrain = "$DeskopChemin\OCARHY Terrain.lnk"
$WScriptShell_t = New-Object -ComObject WScript.Shell

$Shortcut_t = $WScriptShell_t.CreateShortcut($ShortcutTerrain)
$Shortcut_t.TargetPath = $qgis_location
$Shortcut_t.WorkingDirectory = "$qgis_folder"
$Shortcut_t.Arguments = "--profile ocarhy_terrain "+$project_terrain_quote
$shortcut_t.IconLocation=$icon_terrain
$Shortcut_t.Save()

<#
Shortcut Valorisation

$ShortcutValorisation = "$DeskopChemin\OCARHY Valorisation.lnk"
$WScriptShell_t = New-Object -ComObject WScript.Shell

$Shortcut_v = $WScriptShell_t.CreateShortcut($ShortcutValorisation)
$Shortcut_v.TargetPath = $qgis_location
$Shortcut_v.WorkingDirectory = "$qgis_folder"
$Shortcut_v.Arguments = "--profile ocarhy_valorisation "+$project_valorisation_quote
$shortcut_v.IconLocation=$icon_valorisation
$Shortcut_v.Save()
#>

#PARAMETRAGE DE LA SPLASH
#-------------------------------------------------------------------------------------
<#
On va changer le chemin de la splash image
Il faut déjà créer un nouveau chemin vers la splash qui possède des anti-slash donc on doit faire un chercher remplacer "Replace("texte à remplacer","texte de remplacement)"
#>
$splash_bureau = $preset_bureau.Replace('\','/')
$splash_terrain = $preset_terrain.Replace('\','/')
#$splash_valorisation = $preset_valorisation.Replace('\','/')

<#
On détecte la balise "SPLASH_DIRE pour remplacer le texte avec le chemin de la splash
#>
(Get-Content $custo_bureau) -replace '{{ SPLASH_DIR }}',$splash_bureau | Set-Content $custo_bureau
(Get-Content $custo_terrain) -replace '{{ SPLASH_DIR }}',$splash_terrain | Set-Content $custo_terrain
#(Get-Content $custo_valorisation) -replace '{{ SPLASH_DIR }}',$splash_valorisation | Set-Content $custo_valorisation



#PARAMETRAGE DES PROFILS TERRAIN ET BUREAU
#-------------------------------------------------------------------------------------

Set-Variable -Name "qgis3_bureau" -Value $folder_bureau"QGIS3.ini"
Set-Variable -Name "folder_root_bureau" -Value $appdata"ocarhy_bureau"

Set-Variable -Name "qgis3_terrain" -Value $folder_terrain"QGIS3.ini"
Set-Variable -Name "folder_root_terrain" -Value $appdata"ocarhy_terrain"


$dir_bureau = $folder_root_bureau.Replace('\','\\')

(Get-Content $qgis3_bureau) -replace '{{ ICON_DIR }}',$dir_bureau | Set-Content $qgis3_bureau
(Get-Content $qgis3_bureau) -replace '{{ MODEL_DIR }}',$dir_bureau | Set-Content $qgis3_bureau
(Get-Content $qgis3_bureau) -replace '{{ SCRIPT_DIR }}',$dir_bureau | Set-Content $qgis3_bureau


$dir_terrain = $folder_root_terrain.Replace('\','\\')

(Get-Content $qgis3_terrain) -replace '{{ ICON_DIR }}',$dir_terrain | Set-Content $qgis3_terrain
(Get-Content $qgis3_terrain) -replace '{{ MODEL_DIR }}',$dir_terrain | Set-Content $qgis3_terrain
(Get-Content $qgis3_terrain) -replace '{{ SCRIPT_DIR }}',$dir_terrain | Set-Content $qgis3_terrain


#PARAMETRAGE DU PROFIL VALORISATION
#-------------------------------------------------------------------------------------
<#
Set-Variable -Name "qgis3_valorisation" -Value $folder_valorisation"QGIS3.ini"
Set-Variable -Name "folder_root_valorisation" -Value $appdata"ocarhy_valorisation"

$dir_valo = $folder_root_valorisation.Replace('\','\\')

(Get-Content $qgis3_valorisation) -replace '{{ ICON_DIR }}',$dir_valo | Set-Content $qgis3_valorisation
(Get-Content $qgis3_valorisation) -replace '{{ MODEL_DIR }}',$dir_valo | Set-Content $qgis3_valorisation
#>

#CREATION DE LA POP-UP DE FIN
#-------------------------------------------------------------------------------------

<#
New-Object
-ComObject Wscript.Shell

$wshell.Popup("Vous venez d'installer OCARHY avec succès",0,"Félicitation",0x1)
#>

$wshell = New-Object -ComObject Wscript.Shell
$wshell.Popup("OCARHY à bien été installé sur votre machine.`nIl exploite "+$qgis_version,0,"Félicitation",0x1)
Exit
}

else {
<# Message d'erreur classique si l'outil ne s'installe pas s'ouvre #>

$wshell = New-Object -ComObject Wscript.Shell
$wshell.Popup("OCARHY ne détecte pas de version de QGIS 34/36 sur votre machine`n`nVeillez à vérifier qu'une version de QGIS 3.34 (de préférence) a bien été installée sur votre PC et qu'un dossier lui correspondant est présent dans vos dossiers sources : "+$Env:Programfiles,0,"OCARHY n'a pas été installé",0x1)

<# On ferme la pop-up #>
Exit
}